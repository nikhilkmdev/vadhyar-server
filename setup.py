# -*- coding: utf-8 -*-
from distutils.core import setup

from setuptools import find_packages

with open('requirements.txt', 'r') as req_file:
    requirements = req_file.readlines()

setup(
    name='vadhyar-server',
    packages=find_packages(exclude=('tests',)),
    version='2020.10.13',
    description='Python Server which holds APIs for supporting the Client application',
    long_description='Holds all the APIs for supporting the mobile app',
    long_description_content_type="text/markdown",
    author='Nikhil K Madhusudhan (nikhilkmdev)',
    author_email='nikhilkmdev@gmail.com',
    maintainer='Nikhil K Madhusudhan (nikhilkmdev)',
    maintainer_email='nikhilkmdev@gmail.com',
    install_requires=requirements,
    keywords=[],
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.9",
    ],
)
