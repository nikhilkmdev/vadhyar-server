from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint

from vadhyar.api.booking import booking_api
from vadhyar.api.event import user_event_api
from vadhyar.core.activity.activity import activity_api
from vadhyar.core.auth.auth import basic_auth, auth_api
from vadhyar.core.event.event import events_api
from vadhyar.core.provider.provider import provider_api
from vadhyar.core.sect.sect import sect_api
from vadhyar.core.user.user import user_api

app = Flask(__name__)

# Swagger Configurations
swagger_url = '/api/docs'
api_url = '/static/swagger.yaml'
swagger_ui_blueprint = get_swaggerui_blueprint(
    swagger_url,
    api_url,
    config={
        'app_name': "Vadhyar REST APIs"
    }
)
app.register_blueprint(swagger_ui_blueprint, url_prefix=swagger_url)

# Blueprint APIs
app.register_blueprint(sect_api)
app.register_blueprint(events_api)
app.register_blueprint(auth_api)
app.register_blueprint(user_api)
app.register_blueprint(activity_api)
# User action APIs
app.register_blueprint(user_event_api)
app.register_blueprint(provider_api)
app.register_blueprint(booking_api)


@app.route('/')
@basic_auth.login_required
def health():
    return {'status': 'up'}


if __name__ == '__main__':
    app.run(debug=True)
