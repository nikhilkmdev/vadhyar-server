import os
import urllib.parse

from bson.objectid import ObjectId
from pymongo import MongoClient

_CLIENT = None
_DB_BY_NAME = dict()


def get_mongo_client(host=None, port=None, user=None, password=None, tls=False):
    """
    Gets the Mongo DB Client
    :param host: str - the mongo host
    :param port: int - the mongo port
    :param user: str - the user name
    :param password: str - the password
    :param tls: bool - enable/disable SSL/TLS
    :return: the client instance
    """
    host = host or os.getenv('MONGO_HOST')
    host = host or 'localhost'
    port = port or int(os.getenv('MONGO_PORT'))
    port = port or 27017
    user = user or os.getenv('MONGO_USER')
    password = password or os.getenv('MONGO_PWD')
    user = urllib.parse.quote_plus(user) if user else None
    password = urllib.parse.quote_plus(password) if password else None
    global _CLIENT
    _CLIENT = _CLIENT or MongoClient(host, port, username=user, password=password, tls=tls)
    return _CLIENT


def get_db(name, mongo_client=None):
    """
    Gets the DB instance client
    :param name: str - the db name
    :param mongo_client: the mongo client instance, see get_mongo_client
    :return: the db instance
    """
    db = _DB_BY_NAME.get(name)
    if not db:
        client = mongo_client or get_mongo_client()
        db = client[name]
        _DB_BY_NAME[name] = db
    return db


def get_collection(name, db=None, db_name=None):
    """
    Gets the collection to be used for CRUD operations
    :param name: str - the name of the collection
    :param db: the mongo DB client, see get_db
    :param db_name: str - the db name
    :return: return the collection reference
    """
    db = db or get_db(db_name)
    return db[name]


def insert_records(collection_name, records, db=None):
    """
    Inserts records into the given collection
    :param collection_name: str - the name of the collection
    :param records: list - an iterable of records to be inserted
    :param db: the mongo DB client instance
    :return: the result of the insert
    """
    assert isinstance(records, list), "Record(s) must be an iterable"
    collection = get_collection(collection_name, db=db)
    result = collection.insert_many(records)
    return result


def find(collection_name, filter_conditions=None, db=None):
    """
    Finds all the documents, applying filter if provided
    :param collection_name: str - the collection name
    :param filter_conditions: dict - the conditions to use to filter
    :param db: the mongo DB client instance, see get_db
    :return: the records fetched from DB
    """
    collection = get_collection(collection_name, db=db)
    records = collection.find(filter=filter_conditions)
    return records


def find_one(collection_name, filter_conditions=None, db=None):
    """
    Gets the record from the collection, applying the condition if provided
    :param collection_name: str - the collection name
    :param filter_conditions: dict - dictionary consisting of the filters to be applied
    :param db: the mongo DB client instance, see get_db
    :return: the result fetched from DB
    """
    collection = get_collection(collection_name, db=db)
    record = collection.find_one(filter=filter_conditions)
    return record


def find_by_id(collection_name, obj_id, db=None):
    """
    Gets the record from the collection, applying the condition if provided
    :param collection_name: str - the collection name
    :param obj_id: str - the object id to be used to fetch
    :param db: the mongo DB client instance, see get_db
    :return: the result fetched from DB
    """
    record = find_one(collection_name, filter_conditions={'_id': ObjectId(obj_id)}, db=db)
    return record


def update_many(collection_name, new_val_by_attr, filter_conditions=None, db=None):
    """
    Updates the object in the collection name provided, with values specified
    :param collection_name: str - the collection name
    :param new_val_by_attr: dict - new value by attribute
    :param filter_conditions: dict - value by attribute to narrow the records to be updated
    :param db: the db instance, see get_db
    :return: update result
    """
    collection = get_collection(collection_name, db=db)
    result = collection.update_one(
        filter_conditions or {},
        {'$set': new_val_by_attr},
    )
    return result


def update_by_id(collection_name, obj_id, new_val_by_attr, db=None):
    """
    Updates the record in the collection name provided filtered to the id, with values specified
    :param collection_name: str - the collection name
    :param obj_id: str - the object id to update
    :param new_val_by_attr: dict - new value by attribute
    :param db: the db instance, see get_db
    :return: update result
    """
    collection = get_collection(collection_name, db=db)
    result = collection.update_one(
        {'_id': ObjectId(obj_id)},
        {'$set': new_val_by_attr},
    )
    return result


def delete_many(collection_name, filter_conditions=None, db=None):
    """
    Deletes records from the collection, applying the filters if provided
    :param collection_name: str - the collection name
    :param filter_conditions: dict - conditions (value by attribute) to filter on
    :param db: the mongo DB client instance, see get_db
    :return: the result of the delete
    """
    collection = get_collection(collection_name, db=db)
    result = collection.delete_many(filter=filter_conditions)
    return result


def delete_by_id(collection_name, obj_id, db=None):
    """
    Deletes a record based on an object ID
    :param collection_name: str - the collection name
    :param obj_id: str - the object id
    :param db: the mongo DB client instance, get_db
    :return: the result of the delete
    """
    result = delete_many(collection_name, filter_conditions={'_id': ObjectId(obj_id)}, db=db)
    return result


def exists(collection, obj_id, db=None):
    """
    Checks if the a record for the given object id exists
    :param collection: str: the collection name
    :param obj_id: str: the object id
    :param db: the mongo DB client instance, get_db
    :return: True if record exists, False otherwise
    """
    return find(collection, filter_conditions={'_id': ObjectId(obj_id)}, db=db).count() == 1


if __name__ == '__main__':
    c = get_mongo_client(host='192.168.99.100', port=27017)
    d = get_db('test_db', mongo_client=c)
    # i = insert_records('posts', [{'name': 'test', 'idx': 5}], db=d)
    # print(i)
    r = find_one('posts', db=d)
    rs = list(find('posts', db=d))
    # r = delete_by_id('posts', '5f86fc341307dff9953280dc', db=d)
    print(r)
    print(rs)
