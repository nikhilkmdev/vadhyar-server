import os

from bson import json_util
from flask import Blueprint, g, jsonify
from flask_httpauth import HTTPBasicAuth
from itsdangerous import TimedJSONWebSignatureSerializer, SignatureExpired, BadSignature
from passlib.apps import custom_app_context
from werkzeug.exceptions import abort

from vadhyar.db.mongo import find_by_id, get_db, find_one

auth_api = Blueprint('auth_api', __name__, url_prefix='/api/v1/auth')
collection = 'user'
db = get_db('vadhyar')
basic_auth = HTTPBasicAuth()


@auth_api.route('/token/<user_id>', methods=['GET'])
@basic_auth.login_required
def generate_auth_token(user_id):
    """
    Generates an auth token for the given user
    :param user_id: str: the user id
    :return: newly generate token
    """
    expiration = int(os.getenv('TOKEN_EXPIRY', 600))
    app_secret = os.getenv('APP_SECRET', None)
    assert app_secret, abort(400, f'App secret has not been defined')
    s = TimedJSONWebSignatureSerializer(app_secret, expires_in=expiration)
    token = s.dumps({'id': user_id})
    return jsonify({'token': token.decode('ascii'), 'duration': expiration})


@basic_auth.verify_password
def verify_access(access_name, password):
    """
    Verifies the user password
    :param access_name: str: the user name or token
    :param password: str: the password
    :return: user if verified, aborts otherwise
    """
    assert access_name, abort(400, f'For access to be verified token or user name is a must')
    if password:
        user_data = find_one(collection, filter_conditions={'username': access_name}, db=db)
    else:
        user_data = verify_auth_token(access_name)
    assert user_data, abort(400, f'User does not exists')
    user_data.pop('password')
    g.user = user_data
    return json_util.dumps(user_data)


def verify_auth_token(token):
    """
    Verifies the authentication token
    :param token: str: the token to be verified
    :return: user data if verified, None otherwise
    """
    assert token, abort(400, f'Token to be verified cannot be empty')
    app_secret = os.getenv('APP_SECRET', None)
    assert app_secret, abort(400, f'App secret has not been defined')
    s = TimedJSONWebSignatureSerializer(app_secret)
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None  # valid token, but expired
    except BadSignature:
        return None  # invalid token
    user = find_by_id(collection, data['id'], db=db)
    return user


def encrypt_password(secret):
    """
    Gets the encrypted for the secret provided
    :param secret: str: the secret to be hashed
    :return: the hash for the secret
    """
    return custom_app_context.encrypt(secret)


def verify_password(secret, encrypted_password):
    """
    Verifies if the provided secret is the same
    :param secret: str: the secret to be verified
    :param encrypted_password: the encrypted password to verify
    :return: true if secret matches, false otherwise
    """
    return custom_app_context.verify(secret, encrypted_password)
