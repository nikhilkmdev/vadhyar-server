from urllib import request

from bson import json_util
from flask import Blueprint, request
from werkzeug.exceptions import abort

from vadhyar.common.config import CollectionName
from vadhyar.common.util import provided_or_abort_request
from vadhyar.db.mongo import get_db, find, insert_records, find_one, find_by_id, update_by_id, delete_by_id, exists

events_api = Blueprint('events_api', __name__, url_prefix='/api/v1/event')
collection = CollectionName.EVENT.value
db = get_db('vadhyar')


@events_api.route('', methods=['GET'])
def event():
    """
    Gets the events listed in the platform
    :return: the event names
    """
    events = find(collection, db=db)
    return json_util.dumps(events)


@events_api.route('', methods=['POST'])
def add_event():
    """
    Gets the events listed in the platform
    :return: the event names
    """
    event_json = request.json or dict()
    provided_or_abort_request(event_json, ['name', 'serviceTypes'])
    event_name = event_json['name']
    service_types = event_json['serviceTypes']
    event_obj = find_one(collection, filter_conditions={'name': event_name}, db=db)
    not event_obj or abort(400, f'Found an event with the same name: {event_name}')
    event_data = {'name': event_name, 'serviceTypes': service_types}
    result = insert_records(collection, [event_data], db=db)
    return json_util.dumps(result.inserted_ids)


@events_api.route('/<event_id>', methods=['PUT'])
def update_event(event_id):
    """
    Gets the events listed in the platform
    :return: the event names
    """
    event_json = request.json or dict()
    event_obj = find_by_id(collection, event_id, db=db)
    event_obj or abort(400, f'Could not find an event with the id')
    event_name = event_json.get('name')
    service_types = event_json.get('serviceTypes')
    event_data = {'name': event_name} if event_name else dict()
    event_data.update({'serviceTypes': service_types} if service_types else dict())
    result = update_by_id(collection, event_id, event_data, db=db)
    return json_util.dumps(result.modified_count)


@events_api.route('/<event_id>', methods=['DELETE'])
def delete_event(event_id):
    """
    Gets the events listed in the platform
    :return: the event names
    """
    event_obj = find_by_id(collection, event_id, db=db)
    event_obj or abort(400, f'Could not find an event with event id: {event_id}')
    result = delete_by_id(collection, event_id, db=db)
    return json_util.dumps(result.deleted_count)


@events_api.route('/<event_id>/activity/<activity_id>', methods=['LINK'])
def associate_activity(event_id, activity_id):
    """
    Associates the event with activity id provided
    :param event_id: str: the event id
    :param activity_id: str: the activity id
    :return: modified count
    """
    event_data = find_by_id(collection, event_id, db=db)
    event_data or abort(400, f'Event for the given id does not exists')
    is_act_exists = exists(CollectionName.ACTIVITY.value, activity_id, db=db)
    is_act_exists or abort(400, f'Activity for the given id does not exists')
    activities = set(event_data.get('activities', set()))
    activities.update({activity_id, })
    res = update_by_id(collection, event_id, {'activities': list(activities)}, db=db)
    return json_util.dumps(res.modified_count)


@events_api.route('/<event_id>/activity/<activity_id>', methods=['UNLINK'])
def disassociate_activity(event_id, activity_id):
    """
    Disassociates the event with activity id provided
    :param event_id: str: the event id
    :param activity_id: str: the activity id
    :return: modified count
    """
    event_data = find_by_id(collection, event_id, db=db)
    event_data or abort(400, f'Event for the given id does not exists')
    is_act_exists = exists(CollectionName.ACTIVITY.value, activity_id, db=db)
    is_act_exists or abort(400, f'Activity for the given id does not exists')
    _, activities = _remove_activity_from_event(activity_id, event_data)
    res = update_by_id(collection, event_id, {'activities': list(activities)}, db=db)
    return json_util.dumps(res.modified_count)


def remove_activity_from_events(activity_id):
    """
    Removes the activity from all events. Used when an activity is deleted
    :param activity_id: str: the activity id
    :return: modified count
    """
    count = 0
    events = find(collection, db=db)
    for event_data in events:
        event_id, activities = _remove_activity_from_event(activity_id, event_data)
        res = update_by_id(collection, event_id, {'activities': activities}, db=db)
        count += res.modified_count
    return count


def _remove_activity_from_event(activity_id, event_data):
    activities = set(event_data.get('activities', set()))
    activities = activities - {activity_id, }
    return str(event_data['_id']), list(activities)
