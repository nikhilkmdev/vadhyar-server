from bson import json_util
from flask import Blueprint, request
from werkzeug.exceptions import abort

from vadhyar.common.config import CollectionName
from vadhyar.common.util import provided_or_abort_request
from vadhyar.core.auth.auth import basic_auth
from vadhyar.db.mongo import get_db, insert_records, exists, update_by_id, find, delete_by_id

provider_api = Blueprint('provider_api', __name__, url_prefix='/api/v1/provider')
collection = CollectionName.PROVIDER.value
db = get_db('vadhyar')


@provider_api.route('/service_type/<service_type>', methods=['GET'])
@basic_auth.login_required
def get_provider_for_service_type(service_type):
    """
    Gets the providers for the given service type
    :param service_type: str: the service type
    :return: providers matching the service type
    """
    providers = find(collection, filter_conditions={'serviceType': service_type}, db=db)
    return json_util.dumps(list(providers))


@provider_api.route('', methods=['POST'])
@basic_auth.login_required
def add_provider():
    """
    Adds a provider to the platform
    :return: id of the added provider
    """
    provider_json = request.json or dict()
    provided_or_abort_request(provider_json, ['name', 'serviceType', 'description'])
    name = provider_json['name']
    service_type = provider_json['serviceType']
    res = insert_records(collection, [{'name': name, 'serviceType': service_type}], db=db)
    return json_util.dumps(res.inserted_ids)


@provider_api.route('/<provider_id>', methods=['PUT'])
@basic_auth.login_required
def update_provider(provider_id):
    """
    Updates a provider in the platform
    :param provider_id: str: the provider id
    :return: modified count
    """
    is_exists = exists(collection, provider_id, db=db)
    is_exists or abort(400, f'No provider found for the given id')
    provider_json = request.json or dict()
    name = provider_json.get('name')
    service_type = provider_json.get('serviceType')
    description = provider_json.get('description')
    provider_data = {'name': name} if name else dict()
    provider_data.update({'serviceType': service_type} if service_type else dict())
    provider_data.update({'description': description} if description else dict())
    res = update_by_id(collection, provider_id, provider_data, db=db)
    return json_util.dumps(res.modified_count)


@provider_api.route('/<provider_id>', methods=['PUT'])
@basic_auth.login_required
def delete_provider(provider_id):
    """
    Deletes a provider in the platform
    :param provider_id: str: the provider id
    :return: deleted count
    """
    is_exists = exists(collection, provider_id, db=db)
    is_exists or abort(400, f'No provider found for the given id')
    res = delete_by_id(collection, provider_id, db=db)
    return json_util.dumps(res.deleted_count)
