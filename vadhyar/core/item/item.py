from bson import json_util
from flask import Blueprint, request, jsonify
from werkzeug.exceptions import abort

from vadhyar.common.util import provided_or_abort_request
from vadhyar.db.mongo import get_db, find, find_one, insert_records, find_by_id, update_by_id, delete_by_id

item_api = Blueprint('item_api', __name__, url_prefix='/api/v1/item')
collection = 'item'
db = get_db('vadhyar')


@item_api.route('', methods=['GET'])
def items():
    """
    Gets the items currently updated in the platform
    :return: items list
    """
    result = list(find(collection, db=db))
    return json_util.dumps(result)


@item_api.route('', method=['POST'])
def add_item():
    """
    Adds an item into the platform
    :return: the insert id
    """
    item_json = request.json or dict()
    provided_or_abort_request(item_json, ['name', 'price', 'currency', 'unit'])
    name = item_json['name'].lower()
    currency = item_json['currency'].lower()
    unit = item_json['unit'].lower()
    item_obj = find_one(collection, filter_conditions={'name': name, 'currency': currency, 'unit': unit}, db=db)
    assert not item_obj, abort(400, f'Item with the name {name}, currency {currency} and unit {unit} already exists')
    item_data = {
        'name': name,
        'currency': currency,
        'image': item_json['image'],
        'price': item_json['price'],
        'unit': unit,
    }
    result = insert_records(collection, [item_data], db=db)
    return json_util.dumps(result.inserted_ids)


@item_api.route('/<item_id>', method=['PUT'])
def update_item(item_id):
    """
    Updates the item with the given id
    :param item_id: str: the item id
    :return: modified count
    """
    item_obj = find_by_id(collection, item_id, db=db)
    assert item_obj, abort(400, f'Item could not be found for the given id {item_id}')
    item_json = request.json or dict()
    item_data = {attr: item_json[attr] for attr in ['name', 'unit', 'price', 'currency'] if item_json.get(attr)}
    res = update_by_id(collection, item_id, item_data, db=db)
    return jsonify(res.modified_count)


@item_api.route('/<item_id>', method=['DELETE'])
def delete_item(item_id):
    """
    Deletes the item with the given id
    :param item_id: str: the item id
    :return: the deleted count
    """
    item_obj = find_by_id(collection, item_id, db=db)
    assert item_obj, abort(400, f'Item could not be found for the given id {item_id}')
    res = delete_by_id(collection, item_id, db=db)
    return jsonify(res.deleted_count)
