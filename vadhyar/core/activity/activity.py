from logging import info

from bson import json_util
from flask import Blueprint, request, jsonify
from werkzeug.exceptions import abort

from vadhyar.common.config import CollectionName
from vadhyar.common.util import provided_or_abort_request
from vadhyar.db.mongo import get_db, find, find_one, insert_records, find_by_id, update_by_id, delete_by_id, exists
from vadhyar.core.event.event import remove_activity_from_events

activity_api = Blueprint('activity_api', __name__, url_prefix='/api/v1/activity')
collection = 'activity'
db = get_db('vadhyar')


@activity_api.route('', methods=['GET'])
@activity_api.route('/<activity_id>', methods=['GET'])
def activity(activity_id=None):
    """
    Gets all the activities in the platform
    :type activity_id: str: the activity id
    :return: filtered activities
    """
    if activity_id:
        results = find_by_id(collection, activity_id, db=db)
    else:
        results = list(find(collection, db=db))
    return json_util.dumps(results)


@activity_api.route('', methods=['POST'])
def add_activity():
    """
    Adds an activity to the platform
    :return: inserted ids
    """
    activity_json = request.json or dict()
    provided_or_abort_request(activity_json, ['name', 'significance', 'image', 'service'])
    name = activity_json['name']
    act_obj = find_one(collection, filter_conditions={'name': name}, db=db)
    not act_obj or abort(400, f'Activity with the same name already exists')
    activity_data = {
        'name': activity_json['name'],
        'significance': activity_json['significance'],
        'image': activity_json['image'],
        'service': activity_json['service'],
    }
    res = insert_records(collection, [activity_data], db=db)
    return json_util.dumps(res.inserted_ids)


@activity_api.route('<activity_id>', methods=['PUT'])
def update_activity(activity_id):
    """
    Updates the activity for the given activity id
    :param activity_id: str: the activity id
    :return: the updated count
    """
    act_obj = find_by_id(collection, activity_id, db=db)
    act_obj or abort(400, f'No activity found for the activity id')
    act_json = request.json or dict()
    act_data = {act_json[name] for name in ['name', 'significance', 'image', 'service'] if act_json.get(name)}
    res = update_by_id(collection, activity_id, act_data, db=db)
    return jsonify(res.modified_count)


@activity_api.route('<activity_id>', methods=['DELETE'])
def delete_activity(activity_id):
    """
    Deletes the activity for the given activity id
    :param activity_id: str: the activity id
    :return: the deleted count
    """
    act_obj = find_by_id(collection, activity_id, db=db)
    act_obj or abort(400, f'No activity found for the activity id')
    modified_count = remove_activity_from_events(activity_id)
    info(f'Modified {modified_count} events where this event was removed')
    res = delete_by_id(collection, activity_id, db=db)
    return jsonify(res.deleted_count)


@activity_api.route('<activity_id>/item/<item_id>', methods=['LINK'])
def associate_item(activity_id, item_id):
    """
    Associates the item with the activity
    :param activity_id: str: the activity id
    :param item_id: str: the item id
    :return: the modified count
    """
    item_json = request.json
    quantity = item_json.get('quantity')
    quantity or abort(400, f'Quantity must be provided to associate the item')
    activity_data = find_by_id(CollectionName.ACTIVITY.value, activity_id, db=db)
    activity_data or abort(400, f'Could not find an activity for the given id')
    item_exists = exists(CollectionName.ITEM.value, item_id, db=db)
    item_exists or abort(400, f'Could not find an item for the given id')
    qty_by_item = activity_data.get('items', dict())
    qty_by_item[item_id] = float(quantity)
    modified_count = update_by_id(collection, activity_id, {"items": qty_by_item}, db=db)
    return jsonify(modified_count)


@activity_api.route('<activity_id>/item/<item_id>', methods=['UNLINK'])
def disassociate_item(activity_id, item_id):
    """
    Disassociates the item with the activity
    :param activity_id: str: the activity id
    :param item_id: str: the item id
    :return: the modified count
    """
    activity_data = find_by_id(CollectionName.ACTIVITY.value, activity_id, db=db)
    activity_data or abort(400, f'Could not find an activity for the given id')
    item_exists = exists(CollectionName.ITEM.value, item_id, db=db)
    item_exists or abort(400, f'Could not find an item for the given id')
    activity_id, qty_by_item = _remove_item_from_activity(item_id, activity_data)
    modified_count = update_by_id(collection, activity_id, {"items": qty_by_item}, db=db)
    return jsonify(modified_count)


def remove_item_from_activity(item_id):
    """
    Removes the item id from all the activities
    :param item_id: str: the item id
    :return: modified count
    """
    count = 0
    item_exists = exists(CollectionName.ITEM.value, item_id)
    item_exists or abort(400, f'Could not find an item for the given id')
    activities = find(collection, db=db)
    for activity_data in activities:
        activity_id, qty_by_item = _remove_item_from_activity(item_id, activity_data)
        modified_count = update_by_id(collection, activity_id, {"items": qty_by_item}, db=db)
        count += modified_count
    return jsonify(count)


def _remove_item_from_activity(item_id, activity_data):
    qty_by_item = activity_data.get('items', dict())
    item_removed = qty_by_item.pop(item_id, None)
    info(f'Item: {item_removed} was removed from the activity')
    return activity_data['_id'], qty_by_item
