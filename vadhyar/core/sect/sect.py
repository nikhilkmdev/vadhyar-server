from bson import json_util
from bson.json_util import dumps
from flask import Blueprint, request, jsonify
from werkzeug.exceptions import abort

from vadhyar.common.config import CollectionName
from vadhyar.db.mongo import insert_records, find, get_db, find_one, delete_by_id, find_by_id, update_by_id, exists

sect_api = Blueprint('sects_api', __name__, url_prefix='/api/v1/sects')
collection = CollectionName.SECT.value
db = get_db('vadhyar')


@sect_api.route('', methods=['GET'])
@sect_api.route('/<sect_id>', methods=['GET'])
def sect(sect_id=None):
    """
    Gets the sects in the platform
    :type sect_id: str: the sect id
    :return: the sect names
    """
    if sect_id:
        sects = find_by_id(collection, sect_id, db=db)
    else:
        sects = list(find(collection, filter_conditions={'parentId': None}, db=db))
    return dumps(sects)


@sect_api.route('/sub/<sect_id>', methods=['GET'])
def get_sub_sects(sect_id):
    """
    Gets the child sects for the given sect id
    :param sect_id: str: the sect id
    :return: the sects or empty list
    """
    sects = list(find(collection, filter_conditions={'parentId': sect_id}, db=db))
    return json_util.dumps(sects)


@sect_api.route('', methods=['POST'])
def add_sect():
    """
    Adds a new sect to the platform
    :return: the id of the inserted sect
    """
    sect_json = request.json or dict()
    sect_name = sect_json.get('name')
    assert sect_name, abort(400, f'Sect name is a mandatory value')
    sect_obj = find_one(collection, {'name': sect_name}, db=db)
    if sect_obj:
        abort(400, f'Found a sect with the same name: {sect_name}')
    sect_data = {'name': sect_name, 'parentId': sect_json.get('parentId')}
    res = insert_records(collection, [sect_data], db=db)
    return dumps(res.inserted_ids)


@sect_api.route('/<sect_id>', methods=['PUT'])
def update_sect(sect_id):
    """
    Updates the provided sect id
    :param sect_id: str: the sect id
    :return: return the modified count
    """
    assert sect_id, abort(400, f'Sect ID is a mandatory value')
    sect_obj = find_by_id(collection, sect_id, db=db)
    sect_obj or abort(400, f'No sect with the provided id')
    sect_json = request.json or dict()
    sect_data = {name: sect_json[name] for name in ['name', 'parent_id'] if sect_json.get(name)}
    res = update_by_id(collection, sect_id, sect_data, db=db)
    return jsonify(res.modified_count)


@sect_api.route('/<sect_id>', methods=['DELETE'])
def delete_sect(sect_id):
    """
    Delete the sect with the given id
    :param sect_id: str: the sect id
    :return: deleted count
    """
    assert sect_id, abort(400, f'Sect ID is a mandatory value')
    sects = find(collection, filter_conditions={'parent_id': sect_id}, db=db)
    if sects:
        abort(400, f'The sect is a parent for other sects. Delete them first')
    res = delete_by_id(collection, sect_id, db=db)
    return dumps(res.deleted_count)


@sect_api.route('/<sect_id>/event/<event_id>', methods=['LINK'])
def associate_event(sect_id, event_id):
    """
    Associates the sect with the event
    :param sect_id: str: the sect id
    :param event_id: str: the event id
    :return: the modified count
    """
    sect_data = find_by_id(collection, sect_id, db=db)
    is_leaf = is_leaf_sect(sect_id)
    is_leaf or abort(400, f'Cannot associate sect which are parents to other sects')
    is_event_exists = exists('event', event_id, db=db)
    is_event_exists or abort(400, f'Event with given id not found')
    event_ids = set(sect_data.get('events', set()))
    event_ids.update({event_id, })
    res = update_by_id(collection, sect_id, {'events': list(event_ids)}, db=db)
    return json_util.dumps(res.modified_count)


@sect_api.route('/<sect_id>/event/<event_id>', methods=['UNLINK'])
def disassociate_event(sect_id, event_id):
    """
    Disassociates the sect with the event
    :param sect_id: str: the sect id
    :param event_id: str: the event id
    :return: the modified count
    """
    sect_data = find_by_id(collection, sect_id, db=db)
    is_event_exists = exists(CollectionName.EVENT.value, event_id, db=db)
    is_event_exists or abort(400, f'Event with given id not found')
    event_ids = set(sect_data.get('events', set()))
    event_ids = event_ids - {event_id, }
    res = update_by_id(collection, sect_id, {'events': list(event_ids)}, db=db)
    return jsonify(res.modified_count)


def is_leaf_sect(sect_id):
    """
    Checks if the given sect id points to a leaf sect (no child below it)
    :param sect_id: str: the sect id
    :return: True if it is leaf, False otherwise
    """
    sects = list(find(collection, filter_conditions={'parentId': sect_id}, db=db))
    return not bool(sects)
