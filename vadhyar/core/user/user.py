from bson import json_util
from flask import Blueprint, request
from werkzeug.exceptions import abort

from vadhyar.core.auth.auth import basic_auth, encrypt_password
from vadhyar.db.mongo import get_db, find_one, insert_records, find_by_id, delete_by_id

user_api = Blueprint('user_api', __name__, url_prefix='/api/v1/user')
collection = 'user'
db = get_db('vadhyar')


@user_api.route('/<user_id>', methods=['GET'])
@basic_auth.login_required
def user(user_id):
    """
    Gets the user for the given user id
    :param user_id: str: the user id
    :return: the user data
    """
    assert user_id, abort(400, f'')
    user_data = find_by_id(collection, user_id, db=db)
    user_data.pop('password', None)
    return json_util.dumps(user_data)


@user_api.route('', methods=['POST'])
def add_user():
    """
    Adds user to the platform
    :return: username json once added
    """
    username = (request.json or dict()).get('username')
    password = (request.json or dict()).get('password')
    assert username and password, abort(400, f'Username and password cannot be empty')
    user_obj = find_one(collection, filter_conditions={'username': username}, db=db)
    assert not user_obj, abort(400, f'User already exists')
    password = encrypt_password(password)
    user_json = {'username': username, 'password': password}
    res = insert_records(collection, [user_json], db=db)
    resp = {'username': username, 'id': res.inserted_ids} if res.inserted_ids else abort(500, f'Failed to insert user')
    return json_util.dumps(resp)


@user_api.route('/<user_id>', methods=['DELETE'])
def delete_user(user_id):
    """
    Delete user to the platform
    :param user_id: str: the user id
    :return: the deleted count
    """
    user_obj = find_by_id(collection, user_id, db=db)
    assert user_obj, abort(400, f'User does not exists')
    res = delete_by_id(collection, user_id, db=db)
    return json_util.dumps(res.deleted_count)
