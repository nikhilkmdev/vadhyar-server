from enum import Enum


class CollectionName(Enum):
    ACTIVITY = 'activity'
    EVENT = 'event'
    ITEM = 'item'
    PROVIDER = 'provider'
    SECT = 'sect'


class UserEventStatus(Enum):
    PARTIAL_BOOKED = 'partial booked'
    BOOKED = 'booked'
    NEW = 'new'
    COMPLETE = 'complete'


class ServiceType(Enum):
    ITEM = 'item'
    PRIEST = 'priest'
    TRAVEL = 'travel'
