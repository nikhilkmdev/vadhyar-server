from werkzeug.exceptions import abort


def provided_or_abort_request(input_json, mandatory_attrs):
    """
    Checks if the input json has all the mandatory attributes else aborts the flask request
    :param input_json: dictionary of input json
    :param mandatory_attrs: an iterable of attributes to check
    """
    for attr in mandatory_attrs or list():
        assert input_json.get(attr) is not None, abort(400, f'{attr} is a mandatory attribute for this request')
