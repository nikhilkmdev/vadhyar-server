import datetime

from bson import json_util, ObjectId
from flask import Blueprint
from werkzeug.exceptions import abort

from vadhyar.common.config import CollectionName
from vadhyar.core.auth.auth import basic_auth
from vadhyar.db.mongo import get_db, find, insert_records

booking_api = Blueprint('booking_api', __name__, url_prefix='/api/v1/booking')
collection = 'booking'
db = get_db('vadhyar')


@booking_api.route('/<service_type>/provider/<provider_id>/<event_data>')
@basic_auth.login_required
def book_provider(service_type, provider_id, event_date):
    """
    Gets the availability of service providers for the given date
    :param: service_type: str: the service expected
    :param: provider_id: str: the provider id
    :param: event_date: str: the ISO date format
    :return: list of available service providers
    """
    event_date = datetime.datetime.strptime(event_date, '%Y-%m-%d')
    bookings_for_date = list(find(collection, filter_conditions={
        'serviceType': service_type,
        'providerId': provider_id,
        'eventDate': event_date
    }, db=db))
    not bookings_for_date or abort(f'The provider {provider_id} got booked for the date, try a different provider')
    booking_data = {
        'serviceType': service_type,
        'providerId': provider_id,
        'eventDate': event_date
    }
    res = insert_records(collection, [booking_data], db=db)
    return json_util.dumps(res.inserted_ids)


@booking_api.route('/<service_type>/availability/<event_data>')
@basic_auth.login_required
def get_availability(service_type, event_date):
    """
    Gets the availability of service providers for the given date
    :param: service_type: str: the service expected
    :param: event_date: str: the ISO date format
    :return: list of available service providers
    """
    event_date = datetime.datetime.strptime(event_date, '%Y-%m-%d')
    bookings_for_date = find(collection, filter_conditions={
        'serviceType': service_type,
        'eventDate': event_date
    }, db=db)
    provider_ids = {ObjectId(booking['providerId']) for booking in bookings_for_date}
    available_providers = find(CollectionName.PROVIDER.value, filter_conditions={
        'serviceType': service_type,
        '_id': {'$nin': list(provider_ids)},
    }, db=db)
    return json_util.dumps(available_providers)
