from bson import json_util

from vadhyar.api.event import user_event_api
from vadhyar.common.config import CollectionName
from vadhyar.db.mongo import find_by_id, get_db

collection = 'user_event'
db = get_db('vadhyar')


@user_event_api.route('/<activity_id>', methods=['DELETE'])
def get_items_for_activity(activity_id):
    """
    Gets the items for the activity
    :param: str: the activity id
    :return: the item id
    """
    activity_data = find_by_id(CollectionName.ACTIVITY.value, activity_id, db=db)
    return json_util.dumps(activity_data['items'])
