import datetime

from bson import json_util
from flask import Blueprint, g, request
from werkzeug.exceptions import abort

from vadhyar.common.config import CollectionName, UserEventStatus
from vadhyar.common.util import provided_or_abort_request
from vadhyar.core.auth.auth import basic_auth
from vadhyar.db.mongo import get_db, find_by_id, insert_records, exists, update_by_id, find, delete_by_id

user_event_api = Blueprint('user_event_api', __name__, url_prefix='/api/v1/user_event')
collection = 'user_event'
db = get_db('vadhyar')


@user_event_api.route('', methods=['GET'])
@basic_auth.login_required
def user_event():
    """
    Gets the user events created
    :return: list of user events
    """
    user_data = g.user
    user_id = str(user_data['_id'])
    user_events = list(find(collection, filter_conditions={'userId': user_id}, db=db))
    return json_util.dumps(user_events)


@user_event_api.route('/sect/<sect_id>', methods=['GET'])
@basic_auth.login_required
def get_event_for_sect(sect_id):
    """
     Gets the events for the provided sect id
    :param sect_id: str: the sect id
    :return: list of events relevant for the sect
    """
    sect_data = find_by_id(CollectionName.SECT.value, sect_id, db=db)
    sect_data or abort(400, f'Could not find sect for the given id')
    event_ids = sect_data['events']
    event_data = [find_by_id(CollectionName.EVENT.value, event_id, db=db) for event_id in event_ids]
    return json_util.dumps(event_data)


@user_event_api.route('', methods=['POST'])
@basic_auth.login_required
def add_user_event():
    """ Adds an user event """
    user_event_data = request.json
    event_id = user_event_data.get('eventId')
    event_name = user_event_data.get('eventName')
    head_count = user_event_data.get('headCount')
    event_date = user_event_data.get('eventDate')
    provided_or_abort_request(user_event_data, ['eventId', 'eventName', 'headCount', 'eventDate'])
    event_date = datetime.datetime.strptime(event_date, '%Y-%m-%d').date()
    user_event_data = {
        'userId': str(g.user['_id']),
        'eventId': event_id,
        'eventName': event_name,
        'headCount': int(head_count),
        'eventDate': event_date.isoformat(),
        'status': UserEventStatus.NEW.value,
    }
    res = insert_records(collection, [user_event_data], db=db)
    return json_util.dumps(res.inserted_ids)


@user_event_api.route('/<user_event_id>', methods=['PUT'])
@basic_auth.login_required
def update_user_event(user_event_id):
    """
    Updates an user event
    :param user_event_id: str: the user event id
    :return: the number of records modified
    """
    user_event_data = exists(collection, user_event_id, db=db)
    user_event_data or abort(400, f'User event not found for the id')
    user_event_data = request.json
    event_name = user_event_data.get('eventName')
    head_count = user_event_data.get('headCount')
    event_date = user_event_data.get('eventDate')
    event_date = datetime.datetime.strptime(event_date, '%Y-%m-%d').date() if event_date else None
    event_status = user_event_data['status']
    user_event_data = {'eventName': event_name} if event_name else dict()
    user_event_data.update({'headCount': head_count} if head_count else dict())
    can_update_date = event_status not in (UserEventStatus.PARTIAL_BOOKED.value, UserEventStatus.BOOKED.value)
    can_update_date or abort(400, f'Cannot update when service providers are booked')
    user_event_data.update({'eventDate': event_date} if event_date else dict())
    res = update_by_id(collection, user_event_id, user_event_data, db=db)
    return json_util.dumps(res.modified_count)


@user_event_api.route('/<user_event_id>', methods=['DELETE'])
@basic_auth.login_required
def delete_user_event(user_event_id):
    """
    Deletes an user event
    :param user_event_id: str: the user event id
    :return: the number of records deleted
    """
    user_event_data = find_by_id(collection, user_event_id, db=db)
    user_event_data or abort(400, f'User event not found for the id')
    event_status = user_event_data['status']
    can_delete = event_status not in (UserEventStatus.PARTIAL_BOOKED.value, UserEventStatus.BOOKED.value)
    can_delete or abort(400, f'Cannot delete an event which is booked')
    res = delete_by_id(collection, user_event_id, db=db)
    return json_util.dumps(res.deleted_count)


@user_event_api.route('/<user_event_id>', methods=['DELETE'])
@basic_auth.login_required
def update_user_managed_items(user_event_id):
    """
    Updates the user decision to handle the items on their own
    :param user_event_id: str: the user event id
    :return: modified count
    """
    user_managed_item_json = request.json or dict()
    is_user_managed_item_status = user_managed_item_json.get('is_user_managed_item')
    is_user_managed_item_status is not None or abort(400, f'Need the user decision to update')
    user_event_data = find_by_id(collection, user_event_id, db=db)
    user_event_data or abort(400, f'User event not found for the id')
    user_event_data['is_user_managed_items'] = is_user_managed_item_status
    res = update_by_id(collection, user_event_id, user_event_data, db=db)
    return json_util.dumps(res.modified_count)
