from bson import json_util
from flask import request
from werkzeug.exceptions import abort

from vadhyar.api.event import user_event_api
from vadhyar.common.config import CollectionName
from vadhyar.db.mongo import find_by_id, update_by_id, get_db

collection = 'user_event'
db = get_db('vadhyar')


@user_event_api.route('/<user_event_id>/activity/<activity_id>', methods=['PUT'])
def add_selected_activities(user_event_id, activity_id):
    """
    Add the selected activity by user
    :param user_event_id: str: the user event id to update
    :param activity_id: str: the activity id
    :return: modified record count
    """
    user_event_data = find_by_id(collection, user_event_id, db=db)
    selected_act_json = request.json or dict()
    activities = selected_act_json.get('activities', list())
    activity_data = find_by_id(CollectionName.ACTIVITY.value, activity_id, db=db)
    activity_data or abort(400, f'Given activity id: {activity_id} does not exists.')
    user_event_data['activities'] = list(set(activities) | {activity_id, })
    res = update_by_id(collection, user_event_id, user_event_data, db=db)
    return json_util.dumps(res.modified_count)


@user_event_api.route('/<user_event_id>/activity/<activity_id>', methods=['DELETE'])
def delete_selected_activities(user_event_id, activity_id):
    """
    Delete the selected activity by user
    :param user_event_id: str: the user event id to update
    :param activity_id: str: the activity id
    :return: modified record count
    """
    user_event_data = find_by_id(collection, user_event_id, db=db)
    selected_act_json = request.json or dict()
    activities = selected_act_json.get('activities', list())
    activity_data = find_by_id(CollectionName.ACTIVITY.value, activity_id, db=db)
    activity_data or abort(400, f'Given activity id: {activity_id} does not exists.')
    user_event_data['activities'] = list(set(activities) - {activity_id, })
    res = update_by_id(collection, user_event_id, user_event_data, db=db)
    return json_util.dumps(res.modified_count)
